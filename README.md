# cy-api
springboot 接口开发框架

**官网首页  http://www.cymall.xin** 

 **springboot快速开发框架  cy-fast  [https://gitee.com/leiyuxi/cy-fast](https://gitee.com/leiyuxi/cy-fast)** 

 **ssm版开发框架  cy-security  [https://gitee.com/leiyuxi/cy-security](https://gitee.com/leiyuxi/cy-security)** 

 **c纯前端框架  cy-fast  [http://www.cymall.xin:8084](http://www.cymall.xin:8084)** 

 **框架说明** 

1. springboot+mybatis+token机制的接口开发框架！
2. 配置代码生成器，减少70%开发时间，专注业务逻辑。
3. 自定义异常、统一日志处理、redis自定义注解、xss防护..
4. 优化更新中...

    


 **java** 

- api                    接口请求方法

- common     
        - api             接口请求拦截 <br>
        - config         配置类 <br>
        - enumresource   枚举类<br>
        - exception      统一异常处理<br>
        - log            统一日志处理<br>
        - redis          redis相关类<br>
        - utils          工具类<br>
        - xss            xss相关类<br>
       
- controller             其他请求

- dao   
   
- entity   
  
- service


 **resources** 

- doc.db.sql       默认 sql脚本

- generator        代码生成器相关配置（请自行修改代码生成器模板）

- mapper           mapper文件

- static           静态文件

- templates        freemarker页面



 **如何启动**
 
1. 通过git下载源码
2. 创建数据库cy-api，数据库编码为UTF-8，导入 doc-db.sql 
3. IDEA、Eclipse导入项目
4. 启动 ApiApplication类

 **如何使用**

1. 注册： http://localhost:8080/api/register （参数：账号/密码）
2. 登录： http://localhost:8080/api/login     (内置账号：admin/admin)
3. 其他： 请查看demo  ApiTestController.java 



 **如何交流、联系** 

- 官网首页 http://www.cymall.xin
- 开源项目 https://gitee.com/leiyuxi/
- qq群    275846351


 **项目截图** 

swagger接口文档 http://localhost:8080/swagger-ui.html

![输入图片说明](https://gitee.com/uploads/images/2018/0210/113341_a763ffc5_1334796.png "屏幕截图.png")



代码生成器页面截图  http://localhost:8080/gen/list

![输入图片说明](https://gitee.com/uploads/images/2018/0210/114333_00eb071f_1334796.png "屏幕截图.png")





 **捐赠作者** 

如有帮助到您，请作者喝杯咖啡吧！

![输入图片说明](https://gitee.com/uploads/images/2018/0106/184140_fd082023_1334796.png "屏幕截图.png")
