package xin.cymall.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xin.cymall.common.api.annotation.IgnoreAuth;
import xin.cymall.common.api.annotation.LoginUser;
import xin.cymall.common.log.SysLog;
import xin.cymall.common.redis.RedisCache;
import xin.cymall.common.utils.R;
import xin.cymall.entity.User;
import xin.cymall.service.UserService;

import java.util.List;

/**
 * API测试接口
 *
 * @author chenyi
 * @email 228112142@qq.com
 * @date 2017-08-25 08:47
 */
@RestController
@RequestMapping("/api")
public class ApiTestController {
    @Autowired
    private UserService userService;
    /**
     * 获取用户信息
     * @LoginUser 自动注入当前token对应的用户信息
     */
    @GetMapping("userInfo")
    @SysLog(value="获取用户信息")
    public R userInfo(@LoginUser User user){
        return R.ok().put("user", user);
    }

    /**
     * 忽略Token验证测试
     */
    @IgnoreAuth
    @GetMapping("notToken")
    public R notToken(){
        return R.ok().put("msg", "无需token也能访问。。。");
    }



    @PostMapping("list")
    @SysLog(value="获取用户列表")
    @RedisCache(type = User.class)
    public R list(){

       List<User> userList= userService.queryList(null);

        return R.ok().put("userList", userList);
    }

    /**
     * 通过id查询用户信息
     */
    @IgnoreAuth
    @PostMapping("getByUserId")
    @ApiOperation(value="getByUserId", notes="通过id查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true,dataType ="string",paramType = "query"),
    })
    public R getByUserId( @RequestParam( value = "userId") String userId){

        User user= userService.queryObject(userId);

        return R.ok().put("user", user);
    }
}
